program HighDimNewton
    
    use functions
    implicit none
   
    real(8), allocatable :: X0(:), X(:), splendidFunction(:)
    integer(4) :: n
    interface
        function AFunction(X) 
        real(8), dimension(1:), intent(in) :: X
        real(8), dimension(1:size(X)) :: AFunction
        end function AFunction
    end interface

    open(1,file='data.dat')
        read(1,'(2x,i8)') n
    close(1)
    
    allocate(X0(n), X(n), splendidFunction(n))
    
    call Fill(X0)
    call Newton(X0, X, AwesomeFunction)

    deallocate(X0)
    open(2, file='result.dat')
        write(2,*) X
    close(2)
    
    splendidFunction(:) = AwesomeFunction(X)
    write(*,*) '|f(X)|=', sqrt(dot_product(splendidFunction,  splendidFunction))
    deallocate(X)

end program HighDimNewton    